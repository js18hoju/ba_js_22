\section{Die Sprache \texorpdfstring{\jjpci}{PCI}}
Wir wollen nun das logische System \jjpci einführen.
Als erstes legen wir fest,
welche Symbole und Symbolsequenzen zu \jjpci gehören.
Dies ist die Übersetzung von \jjpci in eine formale Sprache.

\medskip
Wir beginnen mit dem \textit{Alphabet von \jjpci}.
Es bestimmt, welche Symbole für Wörter der Sprache verwendet werden dürfen.
\input{./content/text/definitionen/symbole.tex}

Die zulässigen Sequenzen, die aus den Symbolen des Alphabets gebildet werden dürfen,
bestimmen wir mit einer kontextfreien Grammatik.
Dies hat den Vorteil, dass wir zugleich ein Verfahren erhalten,
mit dem Wörter der Sprache erzeugt werden können.
Wir verwenden die übliche Definition einer Grammatik,
wie sie bei \citeauthor{Schöning2008} in \cite[5]{Schöning2008} zu finden ist.
\input{./content/text/definitionen/grammatik.tex}
Durch Alphabet und Grammatik ist nun eindeutig bestimmt, 
welche Wörter zu \jjpci gehören.
Um zu beweisen, dass ein gegebenes Wort zu \jlang\ gehört,
müssen wir einen Weg finden, 
das Wort aus dem Startsymbol \jgrs\ zu erzeugen.
Diesen Weg geben wir mit einem \textit{Syntaxbaum} \cite[7]{Schöning2008} an.
Ein Wort gehört zur Sprache,
genau dann wenn wir seinen Syntaxbaum zeichnen können.
Wie das funktioniert, werden wir im nächsten Abschnitt durch ein Beispiel zeigen.

\countersetBsp
\begin{exmp}
  Betrachten wir nun drei Beispielwörter und
  bestimmen, ob sie zu \jlang\, gehören.
  \begin{align}
    \label{ex1:3}
  & \jimpl{a}{\jimpl{b}{a}}\\
  \label{ex1:1}
  & \jjpcibrac{¶\jja{a}¶}\jjpcibrac{¶\jja{a}¶}\\ 
  \label{ex1:2}
  & \jja{a}\jja{a}
  \end{align} 

  Das Wort \jref{ex1:3} gehört zur Sprache.
  Dies zeigen wir durch die Angabe seines Syntaxbaumes in Abbildung \ref{fig:baum1}.
  Die Wurzel eines Syntaxbaum ist immer das Startsymbol \jgrs. 
  Die Kanten beschriften wir mit derjenigen Regel,
  die auf den vorhergehenden Knoten angewandt wurde.
  Dass wir diesen Baum zeichnen können, ist der Beweis,
  dass \jref{ex1:3} Element 
  von \jlang\ ist.
  \input{./content/text/definitionen/baum}

  Zu beweisen, dass ein Wort \textit{nicht} zu \jlang\ gehört,
  ist oft schwieriger.
  Dazu müssen wir zeigen, dass es \textit{keinen} Weg gibt, 
  seinen Syntaxbaum zu zeichnen.
  Da unsere Regelmenge $P$ unendlich ist, 
  ist es unmöglich alle Möglichkeiten durchzuprobieren.
  Wir müssen schlüssig argumentieren,
  dass es keine Möglichkeit geben kann,
  seinen Syntaxbaum zu zeichnen.

  \jref{ex1:1} ist ein Wort, das nicht zu \jlang\ gehört.
  Hier können wir damit argumentieren, 
  dass es das Symbol ¶ enthält.
  Es ist ¶ $\notin \jAlpha$.
  Also gibt es keine Produktionsregel in \jref{defn:gramm1},
  mit der wir das Symbol erzeugen können.
  Also gilt $\jjpcibrac{¶\jja{a}¶}\jjpcibrac{¶\jja{a}¶} \notin \jlang$.

  Auch das Wort \jref{ex1:2} gehört nicht zu \jlang.
  Es enthält aber nur Symbole des Alphabets, 
  daher müssen wir anders argumetieren.
  Die einzige Möglichkeit \jja{a}\jja{a} zu erzeugen,
  wäre mit Regel \jref{defn:gramm1} aus \jgrs\jgrs.
  Es existiert aber keine Regel um zwei \jgrs\, direkt nebeneinander zu erzeugen.
  \jref{defn:gramm2} erzeugt immer das Symbol \jjpcimplies zwischen ihnen.
  Zur Sicherheit betrachten wir den Syntaxbaum in Abbildung \ref{fig:bsp2}.
  Keine Regel erlaubt es uns, die beiden Teilbäume auf \jgrs\, zurückzuführen.
  Also ist $\jja{a}\jja{a} \notin \jlang$.
  \input{./content/text/definitionen/bsp2}
\end{exmp}

\subsection{Ausdrücke}
Um die Arbeit mit den Wörtern aus \jlang\, zu erleichtern,
verwenden wir Ausdrücke.
Sie ermöglichen es uns,
Aussagen über ganze Wortgruppen zu treffen.
Wenn wir über einen Ausdruck sprechen, 
sprechen wir über alle Wörter,
für die er stehen kann. 
Er ist selbst kein Wort der Sprache \jlang, sondern
steht für ein beliebiges Wort aus einer bestimmten Teilmenge von \jlang.
\input{./content/text/definitionen/ausdrücke}
\input{./content/text/definitionen/exmp_ausdrücke}
Im folgenden werden wir selten von Wörtern reden
und vorallem Aussagen über Ausdrücke treffen.
Diese Aussagen sind aber immer als
Aussagen über alle Wörter zu verstehen,
die aus der Teilmenge von \jlang\ stammen, 
die durch den Ausdruck induziert wird.
\section{Der Kalkül \texorpdfstring{\jjpci}{PCI}}
Ein Kalkül ist ein Regelwerk,
mit dem wir neue Wörter aus Teilmengen der formalen Sprache erzeugen können.
Dies tun wir, indem wir die Wörter \textit{ableiten}.
Dabei verwenden wir eine ausgezeichnete Wortmenge
die \textit{Axiome},
zusammen mit \textit{Schlussregeln}.
Die Bedeutung der involvierten Wörter ist dafür nicht relevant,
wir müssen nur Symbolsequenzen miteinander vergleichen.

\input{./content/text/definitionen/defn_Ableitung}
Diese Definition der Ableitung gilt für Kalküle allgemein, 
sie unterscheiden sich durch ihre Axiome und Schlussregeln.
\input{./content/text/definitionen/defn_kalkül}
Die Axiome von \jjpci haben wir durch Ausdrücke definiert.
Damit sind in \jjpci unendlich viele Wörter Axiome.
In der Literatur wird daher auch von \textit{Axiomschemata} gesprochen.
Jedes Wort, das in dieses Schema passt, ist ein Axiom.

Wir merken an, dass eine Symbolsequenz, wie \jjA{F}\jjAbl\jimpl{C}{D}
für \textit{kein} Wort aus \jlang\ steht.
Es ist eine Aussage, \textit{über} Wörter aus \jlang. 
Sie sagt aus, dass wir für den Ausdruck 
\jimpl{C}{D} eine Ableitung aus den Prämissen \jjA{F}
angeben können.
\footnote{Um hier noch einmal zu den Wörtern zurück zu kommen.
  Der Ausdruck \jimpl{C}{D} steht für Wörter, deren Syntaxbäume mit der Regel \jref{defn:gramm2} beginnen.
  Über alle diese Wörter treffen wir die Aussage. Wollten wir hier präzise sein,
  müssten wir sagen
  \textquote{Wenn ein Wort einen Syntaxbaum
    gemäß dem Ausdruck \jimpl{C}{D} hat, können wir für ihn eine Ableitung aus allen Wortmengen $W$ angeben.
    Die Wortmengen $W$ sind alle,
die für jeden Ausdruck aus \jjA{F}, ein Wort mit entsprechendem Syntaxbaum enthalten.}}
\input{./content/text/definitionen/exmp_abl}

\subsection{Sätze über \texorpdfstring{\jjpci}{PCI}}
Nun wollen wir unseren Kalkül näher untersuchen und 
beweisen dazu einige Sätze, die aus den Definitionen folgen.
In den Sätzen werden somit die Eigenschaften des Kalküls festgehalten.
\input{./content/text/vollständigkeitsbeweis/thm_gfs1}
Nun können wir in folgenden Beweisen,
statt der detaillierten Angabe von \jref{defn:abl1} oder \jref{defn:abl2},
rechts neben einem abgeleiteten Ausdruck, auf diesen Satz verweisen. 

\medskip
Ein Sonderfall der Ableitung besteht, wenn 
die Menge der Prämissen leer ist.
Dann müssen wir uns beim Ableiten 
auf die Fälle \jref{defn:abl1} und \jref{defn:abl3} beschränken.  
Können wir einen Ausdruck \jja{A} aus der leeren Prämissenmenge ableiten,
schreiben wir \jjAbl \jja{A} und nennen ihn ein \textit{Theorem von \jjpci}.
Dass \jimpl{A}{A} ein solches Theorem ist, 
wollen wir nun beweisen.
\input{./content/text/vollständigkeitsbeweis/thm_ReflexivGesetz.tex}

Bei der Ableitung von Theoremen,
sind wir auf keine Prämissen angewiesen.
D.\ h.\ die Prämissenmenge kann beliebig sein.
Dadurch können wir die Theoreme aus beliebigen Prämissen ableiten.
Eine Verallgemeinerung dieser Erkenntnis
zeigen wir mit dem Satz über die Monotonie der Ableitung.
Dieser besagt, 
dass wir keine Ableitungen verlieren,
wenn wir die Menge der Prämissen erweitern.
\input{./content/text/vollständigkeitsbeweis/thm_gfs4.tex}

\jjA{A}\jjAbl\jja{A} bedeutet, 
dass eine Ableitung für \jja{A} 
aus \jjA{A} existiert.
Haben wir einmal eine Ableitung für \jja{A} gefunden,
können wir diese bei späteren Ableitungen wiederverwenden.
Dies ist die Schnittregel der Ableitung und
wir zeigen sie mit folgendem Satz.
\input{./content/text/vollständigkeitsbeweis/thm_gfs2.tex}
Die Bedeutung dieses Satzes liegt darin, 
dass er uns in künftigen Beweisen eine kürzere Schreibweise erlaubt.
Wenn \jjA{A} eine beliebige Ausdrucksmenge ist
und \jjA{A} \jjAbl \jja{A} gilt, 
dann können wir, wenn wir einen Ausdruck
\jjA{A} ableiten wollen, stattdessen aus $\jjA{A} \cup \jja{A}$ ableiten.
Mit anderen Worten,
wir können beim Ableiten aus \jjA{A} den Ausdruck \jja{A} so verwenden, 
als wäre er Teil der Prämissen.
Für die Theoreme bedeutet das, dass wir sie wie Axiome verwenden können.
Dies macht sie besonders nützlich.

In folgenden Beweisen werden wir auf diesen Satz verweisen, 
wenn wir in einem Ableitungschritt einen Ausdruck \jja{A} einsetzen,
für den bekannt ist, dass \jjA{A} \jjAbl \jja{A} gilt.
In diesem Fall ist dann \jja{A} als Abkürzung zu lesen,
für die Folge der Ausdrücke,
die \jja{A} aus
\jjA{A} ableitet. 

\medskip
Laut der Definition \ref{defn_Ableitung},
kann man jeder Ableitung eine feste Länge $n$ zuordnen.
Die Anzahl der Ableitungschritte ist also immer endlich,
auch wenn die Menge der Prämissen unendlich sein sollte,
wir verwenden immer nur eine endliche Teilmenge.
Dies zeigen wir mit folgendem Satz.
\input{./content/text/vollständigkeitsbeweis/thm_gfs5.tex}

Nun kommen wir zu einem zentralen Satz von Kalkülen,
dem Deduktionstheorem.
Dieses besagt, dass, wenn wir einen Ausdruck \jja{B}
durch die Verwendung von \jja{A} aus einer Menge ableiten können,
dann können wir auch
\jimpl{A}{B} ohne die Verwendung von \jja{A} aus der gleichen Menge ableiten.
Wir nennen das Deduktionstheorem aus Gründen der Konvention ein \textit{Theorem}, 
es handelt sich dabei aber um ein Satz über \jjpci.
\input{./content/text/vollständigkeitsbeweis/thm_DeductionTheorem.tex}
Das Deduktionstheorem bringen wir nun in folgendem Lemma zur Anwendung. 
\input{./content/text/vollständigkeitsbeweis/thm_gfs8_lem1}

\subsection{Konsistenz}
Wir nennen eine Ausdrucksmenge konsistent, 
wenn nicht jeder beliebige Ausdruck aus ihr abgeleitet werden kann. 
Diese Eigenschaft ist zentral für unseren Kalkül,
denn nur wenn $\emptyset$ konsistent ist, 
kann er sinnvoll definiert sein.
Um dies einzusehen, stellen wir uns vor
$\emptyset$ wäre inkonsistent.
Wir könnten dann \textit{jedes} Wort aus ihr ableiten.
Mit Satz \ref{t:gfs4} dann auch aus jeder Menge, jedes Wort.
Die Frage, ob ein Wort aus einer Menge ableitbar ist oder nicht, hätte dann keinen Sinn.
Statt einen Kalkül einzuführen könnten wir dann stattdessen auf \jlang\ verweisen.
\input{./content/text/definitionen/defn_Konsistenz.tex}
In Satz \ref{t:gfs4} haben wir gezeigt, 
dass wir keine Möglichkeiten für Ableitungen verlieren,
wenn wir die Menge der Prämissen erweitern,
wir gewinnen höchstens neue Ableitungsmöglichkeiten hinzu.
Darauf folgt,
dass auch Teilmengen einer konsistenten Prämissenmenge,
konsistent sind.
Erweitern wir eine konsistente Prämissenmenge,
ist aber nicht mehr klar, ob sie konsistent bleibt.

Eine Menge die wir nicht mehr erweitern können,
ohne die Konsistenz zu verlieren, 
nennen wir \textit{maximal konsistent}.
\input{./content/text/vollständigkeitsbeweis/defn_MaxKonsistenz.tex}
Maximal konsistente Mengen, haben nützliche Eigenschaften.
Eine davon besteht darin,
dass sie alle Ausdrücke enthalten,
die aus ihnen abgeleitet werden können. 
Diese Eigenschaft nennen wir
\textit{deduktive Abgeschlossenheit} und wir beweisen sie in folgendem Lemma.
\input{./content/text/vollständigkeitsbeweis/thm_MaximalKonsistent.tex}

Eine andere nützliche Eigenschaft der
maximal konsistenten Mengen besteht in ihrer Abgeschlossenheit unter \jref{PCI.MP}.
Wenden wir \jref{PCI.MP} auf einen Ausdruck
einer maximal konsitenten Menge an,
so erhalten wir einen Ausdruck, der wieder Element der Menge ist. 
Dies zeigen wir mit folgendem Satz.
\input{./content/text/vollständigkeitsbeweis/thm_gfs6.tex}

\section{Die Semantik der Sprache \texorpdfstring{\jjpci}{PCI}}
Bisher haben wir mit Wörtern gearbeitet,
ohne deren Bedeutung zu kennen. 
Wir haben beim Ableiten, Regeln auf Symbolsequenzen angewandt und neue Symbolsequenzen 
erhalten. 
Dies haben wir gewissermaßen mechanisch getan.
Man könnte sich ein Computerprogramm vorstellen, dass in der Lage ist,
das selbe zu tun.
Nun wollen wir diesen Rahmen verlassen und den Wörtern  
eine Bedeutung zuweisen.
Dies ist die \textit{Semantik} der Sprache \jjpci.

Wir beginnen mit den Elementen aus \jsvar, 
den \textit{Aussagenvariablen}.
Das sind diejenigen
Wörter aus \jlang, die durch einmalige Anwendung einer Regel aus \jref{defn:gramm1},
auf das Startsymbol, entstanden sind.
Sie bilden die Grundbausteine der Semantik von \jjpci.
Ihre Bedeutung ist, dass sie eine Aussage vertreten.
Mit einer Aussage meinen wir die Bedeutung eines Satzes einer beliebigen Sprache.
Das könnte zum Beispiel auch ein deutscher Satz sein.
Er muss nur derart sein, 
dass wir eindeutig bestimmen können,
ob das, was durch ihn ausgesagt wird \textit{gilt}, oder ob es \textit{nicht gilt}.
Erfüllt ein Satz diese Bedingung, nennen wir ihn eine \textit{Aussage}.

Gilt eine Aussage, sagen wir
sie hat den Wahrheitswert \jjt, ansonsten
hat sie den Wahrheitswert \jjf.
Von jedem Inhalt,
den sie haben könnte,
abstrahieren wir vollkommen und
betrachten einzig und allein ihren Wahrheitswert.
Dies ist bekannt als das \textit{Bivalenzprinzip der Aussagenlogik} \cite[S.7]{Rautenberg2013}.

Die Semantik längerer Wörter 
besteht in der logischen Verknüpfung mehrerer Aussagenvariablen.
Ihr Wahrheitswert wird eindeutig durch die Aussagenvariablen bestimmt,
die in ihnen vorkommen.
Dies ist die \textit{Extensionalität der Aussagenlogik} \cite[S.7]{Rautenberg2013}.

Wir merken an, dass dieser Teil der Semantik 
viel schwerer zu mechanisieren wäre, als ein Kalkül.
Die Identifikation von beliebigen Sätzen mit einer Aussage
kann unter Umständen verlangen, dass wir einen Kontext mit einbeziehen müssen,
oder Ungenauigkeiten der Ausdrucksweise richtig deuten müssen, etc.
Ein Computerprogramm, dass dazu in der Lage wäre, ist schwerer vorstellbar,
als eines für einen Kalkül.

\subsection{Bewertung}
Mit einer \textit{Bewertung} weisen wir den Wörtern der Sprache \jjpci 
einen Wahrheitswert zu.
Um hier mit der gewohnten mathematischen Genauigkeit
weiterarbeiten zu können,
helfen wir uns mit einer speziellen Funktion, der \textit{Belegung}. 
\input{./content/text/definitionen/defn_Belegung}
Wir verzichten darauf den Aussagenvariablen irgendwelche Sätze zuzuordnen und
dann zu ermitteln, ob diese Sätze nun gelten oder nicht,
sondern weisen ihnen direkt und eindeutig einen Wahrheitswert zu.
Mit der Angabe einer Belegung überspringen wir 
alle Schritte, die mit Ungewissheit verbunden sind.
Damit können wir nun die Bewertung definieren.

\input{./content/text/definitionen/defn_bewertung}
In \jref{defn:bewertung2} tauchen bekannte Symbole in einem
neuen Zusammenhang auf.
Die Symbole haben nun eine Bedeutung.
Die Klammern legen die Rechenreihenfolge fest 
und das Symbol \textquote{\jjpcimplies} ist ein Operator.
Die Bewertung transformiert den Syntaxbaum in einen Operatorbaum.

Hier bilden \jbell{\jja{B}} und \jbell{\jja{C}} auf Wahrheitswerte ab, 
das Symbol \textquote{\jjpcimplies} ist also eine Verknüpfung von Wahrheitswerten.
Diese definieren wir nun.
\input{./content/text/definitionen/pci_impl}
Wenn wir wissen, wie eine Bewertung abbildet, 
dann kennen wir
die zugrunde liegende Belegung.
Andersherum, wenn uns eine Belegung gegeben ist, wissen wir auch,
wie die von ihr induzierte Bewertung abbildet.
Daher werden wir im folgenden auf eine Unterscheidung verzichten und vor allem von 
Bewertungen sprechen.
Wenn wir sagen \textquote{Sei \jbew\ eine beliebige Bewertung},
dann meinen wir \textquote{Sei \jbel\ eine beliebige Belegung und \jbew\ die von ihr 
induzierte Bewertung}.
\input{./content/text/definitionen/exmp_bewertung}
Die Axiome hatten wir als Grundannahmen über unser logisches System motiviert.
Da wir nun die Semantik kennengelernt haben, wollen wir die Axiome nun unter diesem Blickwinkel erneut betrachten.
In \jjpci machen wir zwei Grundannahmen über die Eigenschaften der Implikation
und eine allgemeiner Art. Sie sind:
\begin{description}
  \item[Grundannahme 1.] 
    \textcquote[§14]{Frege2014}{Wenn ein Satz \jja{a} gilt, so gilt er
    auch, falls ein beliebiger Satz \jja{b} gilt.}\\
    Diese Formulierung geht auf Freges Begriffschrift zurück.
    Das Axiom \jref{PCI.A1}
    ist die Übersetzung dieser Grundannahme in ein Wort der Sprache \jjpci.
    Der Zusammenhang wird deutlich, 
    wenn wir das Axiom ausformulieren zu
    \textquote{Wenn \jja{a} gilt, dann gilt $[$ wenn \jja{b} gilt, dann gilt \jja{a} $]$.}

  \item[Grundannahme 2.] 
    \textcquote[§14]{Frege2014}{Wenn ein Satz \jja{a} die nothwendige Folge von zwei Sätzen (\jja{b} und \jja{c}) ist [...], und wenn der eine von ihnen \jja{b} wieder die nothwendige Folge des andern \jja{c} ist, so ist der
    Satz \jja{a} die nothwendige Folge dieses letzten \jja{c} allein.}\\
    Auch diese Formulierung geht auf Frege zurück und legt fest, wie eine Implikation
    beschaffen sein sollte.
    Das Axiom \jref{PCI.A2}
    übersetzt diese Grundannahme in ein Wort der Sprache \jjpci.

  \item[Grundannahme 3.] 
    Das Gesetz des ausgeschlossenen Dritten.
    \cite[Vol.3, S.384]{Peirce1974}\\
    Dieses Prinzip ist ein Annahme über das logische System allgemein und hat 
    nichts mit der Implikation zu tun.
    In \jjpci haben wir aber nur die Implikation zur Verfügung,
    um Wörter zu bilden.
    Die Übersetzung dieses Prinzips in die implikative Gestalt,
    geht auf Peirce zurück und ist daher auch als Peirces Law bekannt.
    Das Axiom \jref{PCI.A3}
    ist die Übersetzung dieser Grundannahme in ein Wort der Sprache \jjpci.
\end{description}

Vorallem beim Betrachten von Peirces Law kann man sich fragen,
wieso genau diese Axiome die richtigen 
für unser Kalkül sein sollten.
Da wir die Richtigkeit der Axiome dadurch bestimmen, 
ob sie den Kalkül vollständig machen,
wird einsichtig, dass wir genauso gut auch andere Axiome verwenden könnten.
Insbesondere könnten wir jedes Theorem zu den Axiomen hinzufügen, ohne die Vollständigkeit
des Kalküls zu beeinflussen.
Es ist aber auch möglich, den Kalkül mit nur einem einzigen Axiom vollständig zu machen.
\citeauthor{Shortest} hat in \cite{Shortest} gezeigt, 
dass dazu dieses Axiom ausreichen würde:
\begin{align}
  \jimpl{\jimpl{\jimpl{A}{B}}{C}}{\jimpl{\jimpl{C}{A}}{\jimpl{D}{A}}} \tag{A.L}\label{PCI.AL}
\end{align}
Dies beweist er durch die Ableitung 
von \jref{PCI.A1}, \jref{PCI.A2} und \jref{PCI.A3} aus \jref{PCI.AL}.
Dass wir unsere Axiome in dieser Arbeit bevorzugen liegt daran,
dass in ihnen die Motivation durch die Grundannahmen noch zu erkennen ist.

\subsection{Folgerung}
In der Semantik von \jjpci können wir das logische Schließen
nun auf eine neue Art formalisieren.
Dies tun wir im Begriff der \textit{Folgerung}.
\input{./content/text/definitionen/defn_Folgerung}
Um einen Ausdruck \jja{B} mit \jjA{A}\jjfolg\jja{B} zu finden,
müssten wir zuerst alle Bewertungen auflisten, die alle Wörter in 
\jjA{A} zu \jjt auswerten und dann kontrollieren, 
ob alle diese Bewertungen 
auch \jja{B} zu \jjt auswerten.
Wenn in den Ausdrücken
insgesamt $n$ verschiedenen Aussagenvariablen vorkommen,
müssen wir dafür $2^n$ Zeilen einer Wahrheitswertetabelle kontrollieren.

Zwei Eigenschaften der Folgerung wollen wir nun in den folgenden Lemmata zeigen.
\input{./content/text/definitionen/folg_tm}
\input{./content/text/definitionen/folg_axiom}
\section{Adäquatheit des Kalküls \texorpdfstring{\jjpci}{PCI}}
Ein Kalkül nennen wir \textit{adäquat},
wenn die Folgerungsbeziehung der Ableitungsbeziehung entspricht.
Das, was aus einer Ausdrucksmenge folgerbar ist, 
sollte dem entsprechen, was aus ihr ableitbar ist.
Diese Eigenschaft lässt sich aufteilen in
die \textit{Korrektheit} und \textit{Vollständigkeit} eines Kalküls.
Ein Kalkül ist adäquat, genau dann wenn er korrekt und vollständig ist.

Unser Ziel ist nun, die Adäquatheit des Kalküls \jjpci zu zeigen.
Eine erste Intuition können wir 
erhalten,
wenn wir die beiden vorherigen Lemmata über die Folgerung 
(Lemma \ref{thmfolgtm} und Lemma 
\ref{thmfolgaxiom}) mit dem Satz \ref{t:gfs1} über die Ableitung vergleichen.
Wir beginnen nun mit der Korrekteit des Kalküls.

\subsection{Korrektheitsbeweis}
Die Korrektheit gibt eine obere Schranke an, 
für das, was alles abgeleitet werden können soll.
Es sollen nur diejenigen Ausdrücke sein, 
die wir auch folgern können und kein Ausdruck mehr.
\input{./content/text/definitionen/thm_korrekt}

\subsection{Vollständigkeitsbeweis}
Mit der Korrekteit ist aber noch nicht sicher gestellt, 
dass wir auch für \textit{alle} Ausdrücke, die gefolgert werden können,
eine Ableitung finden.
Das ist im Begriff der Vollständigkeit eines Kalküls gefasst.
Die Vollständigkeit bildet also eine untere Schranke für das was alles abgeleitet werden können soll.
Es sollen mindestens die Wörter sein, die wir folgern können.

Jetzt werden wir beweisen, dass der Kalkül \jjpci vollständig ist und
dazu die Kontraposition
\textit{wenn \jjA{A} \jjNAbl \jja{A}, dann
\jjA{A} \jjnfolg \jja{A}} zeigen.
Der Beweis wird sich eine Eigenschaft der maximal 
konsistenten Ausdurcksmengen zu Nutze machen.
Diese besteht darin, dass jede maximal konsistente Ausdrucksmenge,
immer eine Bewertung hat,
die alle in ihr enthaltenen Ausdrücke zu \jjt auswertet.
Diese Eigenschaft zeigen wir in Satz \ref{t:gfs7}.

Damit wir Satz \ref{t:gfs7} auch auf beliebige Ausdrucksmengen
anwenden können,
zeigen wir in Satz \ref{t:gfs8}, 
wie wir eine Ausdrucksmenge zur Maximalität erweitern können.
Die Kombination beider Sätze ergibt dann
den eigentlichen Vollständigkeitsbeweis in
Satz \ref{t:gfs9}.
\input{./content/text/vollständigkeitsbeweis/thm_gfs7}
\input{./content/text/vollständigkeitsbeweis/thm_gfs8}
\input{./content/text/vollständigkeitsbeweis/thm_9}

Satz \ref{thm:Korrektheit} und Satz \ref{t:gfs9} ergeben zusammen
die Adäquatheit
des Kalküls \jjpci.
Wir wissen also, dass die semantische Folgerung genau der syntaktischen Ableitung entspricht. 
Eine direkte Folge davon ist, dass die Theoreme genau den Tautologien entsprechen.
Dies gibt uns die Sicherheit, dass die Axiome und die Schlussregel richtig gewählt sind.
In gewisser Weise enthalten sie den vollständigen Sinn unseres logischen Systems.

\medskip
Die Idee, die Schumm in seinem Beweis verwendet,
geht auf eine Methode zurück,
die in der Literatur als \textit{Henkin-Beweis} bekannt ist.
Sie wurde im Jahr 1949 von Leon Henkin
in \cite{LHComplete} vorgestellt.
Dort zeigt Henkin die Vollständigkeit der Prädikatenlogik erster Stufe.
Sie wurde zuvor schon von Gödel in \cite{Gödel} bewiesen,
aber Henkins 
Verfahren zeichnet sich dadurch aus, dass
es weniger Vorbereitung bedarf.
Es ist daher auch häufiger als Gödels Beweis
in aktuellen Lehrbüchern zu finden.
Zum Beispiel in den Arbeiten von Rautenberg \cite[76]{RautenbergEinführung},
Ebbinghaus \cite[79]{HEEinf}
und Beckermann \cite[305]{Beckermann2014}.

In \cite{PollockHenkinStyle} wandelte dann
John L.\ Pollock das Beweisverfahren Henkins
dahingehend ab,
dass es auch ohne den Bezug auf die logische Negation auskommt.
Diese war bei Henkin noch fester Bestandteil des Beweises.
Pollock zeigt darin die Vollständigkeit eines Fragments der Aussagenlogik,
welches keine Negation enthält aber die Disjunktion.
Er zeigte damit auch, wie flexibel Henkins Verfahren ist.
Mit leichten Modifikationen lässt es sich auf andere logische 
Systeme anwenden.

In diesem Sinne stellt Schumms Paper,
eine weitere Modifikation von Henkins Verfahren dar.
Schumm zeigt, dass ein System lediglich die logische Implikation enthalten muss, um für Henkins Verfahren zugänglich zu sein.
Ein System, dass nur noch die Implikation enthält,
ist die ein deduktives System schlechthin 
und von besonderer Bedeutung, denn es ist in vielen komplexeren Logiken, als Grundlage enthalten 
und seit Aristoteles Syllogismus fester Bestandteil der Logik.
