\countersetSatz
\begin{thm}\label{t:gfs8}
  Sei \jjA{A} beliebig.
  Wenn
  \jjA{A} \jjNAbl \jja{A},
  dann existiert eine maximal konsistente Erweiterung \jjA{E} von \jjA{A},
  mit 
  $\jja{A}\notin\jjA{E}$. 
\end{thm}

\begin{proof}
  Sei 
  \jjA{A} \jjNAbl \jja{A}.
  Wir werden die maximal konsistente Erweiterung \jjA{E} schrittweise konstruieren.
  Dazu nummerieren wir alle Ausdrücke nach ihrer
  lexikografischen Ordnung durch, wobei 
  $\jja{A} <  \jja{B} < \ldots < \jja{Y} < \jja{Z} < \jjpcibrac{<} < \jjpcimplies$.
  Sei \jja{X_1},  \jja{X_2},  \jja{X_3}, \ldots \; die Folge aller Ausdrücke, geordnet nach dieser Nummerierung.
  Wir werden \jjA{A} zunächst zur Menge
  $\jjA{A}_{\omega}$ erweitern.
  $\jjA{A}_{\omega}$ definieren wir induktiv.
  \begin{align}
        &\jjA{A}_0 \hspace{0.8em} &&=&& \jjA{A}\\
        &
    \jjA{A}_{n+1}&&=&& 
    \jjA{A}_n\cup\left\{\jimpl{A}{X_{n+1}}\right\}
    \label{omega+1}\\
                 &\jjA{A}_{\omega} &&=&& \bigcup\limits_{n=0}^{\infty} \jjA{A}_n
  \end{align}
  Dass unsere Erweiterung alle Ausdrücke aus
  $\jjA{A}_{\omega}$ enthält,
  wird im späteren Verlauf des Beweises noch wichtig werden.
  Zunächst stellen wir sicher,
  dass \jja{A} durch das Hinzufügen dieser
  Ausdrücke nicht ableitbar geworden ist.

  \begin{lem}\label{kor:1}
    $\text{Für alle $n$ gilt }
    \jjA{A}_n \jjNAbl \jja{A}$
  \end{lem}
  \begin{proof}
    Wir zeigen das Lemma per Induktion über $n$.

    Bei $n=0$ ist 
    $\jjA{A}_0 = \jjA{A}$ und
    laut Annahme ist 
    \jjA{A}\jjNAbl\jja{A}.

    Sei nun bis für alle 
    $n \leq k$,
    $\jjA{A}_n \jjNAbl \jja{A}$.
    Zu zeigen ist, dass dann auch 
    $\jjA{A}_{k+1} \jjNAbl \jja{A}$.

    Laut Annahme ist
    $\jjA{A}_k \jjNAbl \jja{A}$.
    Mit Lemma \ref{l:gfs1} gilt dann
    $\jjA{A}_{k} \cup \left\{\jimpl{A}{X_{k+1}}\right\} \jjNAbl \jja{A}$
    Laut Definition \ref{omega+1} ist 
    $\jjA{A}_{k+1} =
    \jjA{A}_k\cup\left\{\jimpl{A}{X_{k+1}}\right\}$.

    Mit dem Induktionsprinzip gilt dann für alle $n$, dass
    $\jjA{A}_n \jjNAbl \jja{A}$.
  \end{proof}

  Im nächsten Schritt fügen wir die übrigen Ausdrücke hinzu,
  die unserer Erweiterung noch zur Maximalität fehlen.
  Dazu definieren wir die Ausdrucksmenge
  $\jjA{D}_{\omega}$ induktiv.
  \begin{align}
      &\jjA{D}_0 &&=&&\jjA{A}_{\omega}&	\\
      &\jjA{D}_{i+1} &&=&&
      \begin{cases}
        \jjA{D}_i \cup \{ \jja{X}_i \}  & \text{, wenn }	\jjA{D}_i \cup \{ \jja{X}_i \} \text{\ konsistent}\label{konstest}\\
        \jjA{D}_i & \text{, sonst}	\\
      \end{cases}\\
      &\jjA{D}_{\omega} &&=&& \bigcup\limits_{i=0}^{\infty} \,\jjA{D}_i
  \end{align}

  \begin{lem}\label{kor:2}
    Für alle $i$ ist $\jjA{D}_{i}$ konsistent.
  \end{lem}
  \begin{proof}
    Angenommen 
    $\jjA{D}_0$
    wäre inkonsistent.
    Dann wäre 
    $\jjA{D}_{0}\jjAbl\jja{A}$.
    Es ist
    $\jjA{D}_{0} =\jjA{A}_{\omega}$,
    also wäre dann auch
    $\jjA{A}_{\omega}\jjAbl\jja{A}$.
    Wir wissen dass bei 
    $\jjA{A}_{0}$ noch
    $\jjA{A}_{0}\jjNAbl\jja{A}$ galt.
    Also muss es irgendein $n$ geben, 
    bei dem 
    $\jjA{A}_{n}\jjAbl\jja{A}$ wurde.
    Das steht im Widerspruch zu Lemma \ref{kor:1}.
    Also muss
    $\jjA{D}_0$ konsistent sein.
    Laut der Definition \ref{konstest}
    ist 
    $\jjA{D}_i$ 
    also auch konsistent für alle $i$.
  \end{proof}

  \begin{lem}
    $\jjA{D}_{\omega}$ 
    ist maximal konsistent und 
    $\jja{A}\notin\jjA{D}_{\omega}$ 
  \end{lem}
  \begin{proof}
    Dies zeigen wir erneut indirekt.
    Sei dazu 
    $\jjA{D}_{\omega}$
    inkonsistent oder 
    $\jja{A} \in \jjA{D}_{\omega}$.

    Wenn 
    $\jjA{D}_{\omega}$
    inkonsistent ist, gilt
    $\jjA{D}_{\omega} \jjAbl \jja{A}$.
    Mit Satz \ref{t:gfs1} gilt dies auch, 
    wenn $\jja{A} \in \jjA{D}_{\omega}$.
    Also ist in jedem Fall
    $\jjA{D}_{\omega} \jjAbl \jja{A}$.
    Satz \ref{t:gfs5} besagt,
    dass es eine endliche Teilmenge von
    $\jjA{D}_{\omega}$
    geben muss,
    von der \jja{A} abgeleitet werden kann.
    Satz \ref{t:gfs4} besagt, dass auch 
    wenn wir dieser Teilmenge weitere Ausdrücke hinzufügen,
    bleibt 
    \jja{A} ableitbar. 
    Also wird auch ein
    $k$ existieren mit
    $\jjA{D}_{k}\jjAbl\jja{A}$.
    Da $\jjA{D}_{k}$ eine Erweiterung von 
    $\jjA{A}_{\omega}$
    ist, 
    enthält es 
    \jimpl{A}{X_i},
    für alle $i$.
    Wir können also alle Ausdrücke 
    $\jja{X}_i$,
    mit \jref{PCI.MP} aus ihr ableiten.
    Damit ist
    $\jjA{D}_{k}$
    inkonsistent.
    Dies steht im Widerspruch zu Lemma \ref{kor:2}.
    Also muss 
    $\jjA{D}_{\omega}$ 
    maximal konsistent und 
    $\jja{A}\notin\jjA{D}_{\omega}$ 
    sein.
  \end{proof}

  Dies beschließt unseren Beweis und wir wissen,
  dass
  $\jjA{D}_{\omega}$ die
  gesuchte Erweiterung von \jjA{A} ist.
\end{proof}
